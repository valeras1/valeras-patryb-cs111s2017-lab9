import java.io.IOException;
import java.util.Scanner;
import java.util.Iterator;

public class TodoListMain {

    public static void main(String[] args) throws IOException {
        System.out.println("Welcome to the Todo List Manager!");
        System.out.println("What operation would you like to perform?");
        System.out.println("Available options: read, priority-search, category-search, done, list, quit");

        Scanner scanner = new Scanner(System.in);
        TodoList todoList = new TodoList();

        todoList.readTodoItemsFromFile();
	// reads what is within to do list if asked to read
        System.out.print(">");
        while(scanner.hasNext()) {

            String command = scanner.nextLine();
            if(command.equals("read")) {
                todoList.readTodoItemsFromFile();
            }
	// lists what was called for if asked to list
            else if(command.equals("list")) {
                System.out.println(todoList.toString());
            }
	// asks for and outputs priority if asked for priority-search
            else if(command.equals("priority-search")) {
                System.out.println("What is the priority?");
                String requestedPriority = scanner.nextLine();
                Iterator priorityIterator = todoList.findTasksOfPriority(requestedPriority);
                while(priorityIterator.hasNext()) {
                    System.out.println((TodoItem)priorityIterator.next());
                }
	// asks for and outputs category if asked for category-search
            }
            else if(command.equals("category-search")) {
                System.out.println("What is the category?");
                String requestedCategory = scanner.nextLine();
                Iterator categoryIterator = todoList.findTasksOfCategory(requestedCategory);
                while(categoryIterator.hasNext()) {
                    System.out.println((TodoItem)categoryIterator.next());
                }
	// marks a certain task as done if asked for done
            }
            else if(command.equals("done")) {
                System.out.println("What is the id of the task?");
                int chosenId = scanner.nextInt();
                todoList.markTaskAsDone(chosenId);
            }
	// ends Todo List Manager by stating quit
            else if(command.equals("quit")) {
                break;
            }
            System.out.print(">");
        }

    }

}
