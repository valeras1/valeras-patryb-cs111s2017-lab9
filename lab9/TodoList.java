import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class TodoList {

  private ArrayList<TodoItem> todoItems;
  private static final String TODOFILE = "todo.txt";

  public TodoList() {
    todoItems = new ArrayList<TodoItem>();
  }

  public void addTodoItem(TodoItem todoItem) {
    todoItems.add(todoItem);
  }

  public Iterator getTodoItems() {
    return todoItems.iterator();
  }

//Reads and processes each priority or category
  public void readTodoItemsFromFile() throws IOException {
    todoItems.clear();
    Scanner fileScanner = new Scanner(new File(TODOFILE));
    while(fileScanner.hasNext()) {
      String todoItemLine = fileScanner.nextLine();
      Scanner todoScanner = new Scanner(todoItemLine);
      todoScanner.useDelimiter(",");
      String priority, category, task;
      priority = todoScanner.next();
      category = todoScanner.next();
      task = todoScanner.next();
      TodoItem todoItem = new TodoItem(priority, category, task);
      todoItems.add(todoItem);
    }
  }

// Reads and processes when user marks a task as done
  public void markTaskAsDone(int toMarkId) {
    Iterator iterator = todoItems.iterator();
    while(iterator.hasNext()) {
      TodoItem todoItem = (TodoItem)iterator.next();
      if(todoItem.getId() == toMarkId) {
        todoItem.markDone();
      }
    }
  }
// Possibly implement while loops for next two, possible Array List to store in which each priority/category is, then a while loop to output what is called for
  public Iterator findTasksOfPriority(String requestedPriority) {
     Iterator iterator = todoItems.iterator();
     ArrayList<TodoItem> priorityList = new ArrayList<TodoItem>();
     while(iterator.hasNext()) {
        TodoItem todoItem = (TodoItem)iterator.next();
	      if(todoItem.getPriority().equals(requestedPriority)) {
	         priorityList.add(todoItem);
         }
      }
    return priorityList.iterator();
  }

  public Iterator findTasksOfCategory(String requestedCategory) {
     Iterator iterator = todoItems.iterator();
    ArrayList<TodoItem> categoryList = new ArrayList<TodoItem>();
    while(iterator.hasNext()) {
    	TodoItem todoItem = (TodoItem)iterator.next();
	    if(todoItem.getCategory().equals(requestedCategory)) {
        categoryList.add(todoItem); }
}
    return categoryList.iterator();
  }

  public String toString() {
    StringBuffer buffer = new StringBuffer();
    Iterator iterator = todoItems.iterator();
    while(iterator.hasNext()) {
      buffer.append(iterator.next().toString());
      if(iterator.hasNext()) {
        buffer.append("\n");
      }
    }
    return buffer.toString();
  }

}
