public class TodoItem {

  private int id;
  private static int nextId = 0;
  private String priority;
  private String category;
  private String task;
  private boolean done;

  public TodoItem(String p, String c, String t) {
   //Starts ID at 0 and adds 1 for each one
    id = nextId;
    nextId++;
    priority = p;
    category = c;
    task = t;
    done = false;
  }
// returns ID
  public int getId() {
    return id;
  }
// returns Priority
  public String getPriority() {
    return priority;
  }
// returns Category
  public String getCategory() {
    return category;
  }
// returns task
  public String getTask() {
    return task;
  }
// marks task as done
  public void markDone() {
    done = true;
  }

  public boolean isDone() {
    return done;
  }

// Order of output, which is ID, Priority, Category, Task, and Completion; returns what the user outputs in this order
  public String toString() {
    return new String(id + ", " + priority + ", " + category + ", " + task + ", done? " + done);
  }
}
